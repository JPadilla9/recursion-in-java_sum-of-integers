This program finds the product of a and b recursively.
*Base case = b=0, c=0
		     a=1, c=b
*General Solution = a + (a * (b-1))
This one iterates through the reduced problem of ab starting at b = 6. Then when it gets to 1. It iterates backward for the general solution of a + (a*(b-1))
